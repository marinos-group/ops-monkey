export const apps = [
  {
    id: "1",
    remoteProjectId: "Project-1",
    activeDeployments: [
      {
        id: 2,
        referenceLink: "https://reference/2",
        deployedOn: "https://deployed/2",
        active: true,
        managedApplicationId: 1,
        environment: {
          id: 2,
          name: "master",
        },
      },
      {
        id: 3,
        referenceLink: "https://reference/3",
        deployedOn: "https://deployed/3",
        active: true,
        managedApplicationId: 1,
        environment: {
          id: 3,
          name: "stage",
        },
      },
      {
        id: 4,
        referenceLink: "https://reference/4",
        deployedOn: "https://deployed/4",
        active: true,
        managedApplicationId: 1,
        environment: {
          id: 10,
          name: "review/4788c132",
        },
      },
    ],
  },
];

export const envs = [
  {
    id: "1",
    name: "master",
    managedApplicationId: "1",
    deployments: [
      {
        id: 2,
        referenceLink: "https://reference/2",
        deployedOn: "https://deployed/2",
        active: true,
      },
      {
        id: 3,
        referenceLink: "https://reference/3",
        deployedOn: "https://deployed/3",
        active: false,
      },
      {
        id: 4,
        referenceLink: "https://reference/4",
        deployedOn: "https://deployed/4",
        active: false,
      },
    ],
  },
  {
    id: "2",
    name: "stage",
    managedApplicationId: "1",
    deployments: [
      {
        id: 5,
        referenceLink: "https://reference/5",
        deployedOn: "https://deployed/5",
        active: true,
        environment: {
          id: 2,
          name: "stage",
        },
      },
      {
        id: 6,
        referenceLink: "https://reference/6",
        deployedOn: "https://deployed/6",
        active: false,
        environment: {
          id: 2,
          name: "stage",
        },
      },
      {
        id: 7,
        referenceLink: "https://reference/7",
        deployedOn: "https://deployed/7",
        active: false,
        environment: {
          id: 2,
          name: "stage",
        },
      },
    ],
  },
];
