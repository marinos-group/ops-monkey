export default function Titlebar({ primaryText, secondaryText, sideElement }) {
  const renderSecondary = () => {
    if (!secondaryText) {
      return null;
    }
    return (
      <div className="-mt-1 w-5/12 italic text-sm text-gray-700">
        <div>{secondaryText}</div>
      </div>
    );
  };
  return (
    <div className="w-full flex flex-col">
      <div className="w-full flex flex-row justify-between">
        <div className="my-auto text-2xl font-semibold">{primaryText}</div>
        <div className="my-auto">{sideElement}</div>
      </div>
      {renderSecondary()}
    </div>
  );
}
