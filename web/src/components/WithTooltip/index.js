import { createPopper } from "@popperjs/core";
import { useCallback, useEffect, useRef, useState } from "react";
import "./styles.css";

export default function WithTooltip({ text, placement, children }) {
  const tooltip = useRef();
  const parent = useRef();
  const [show, setShow] = useState(false);

  const initPopper = useCallback(() => {
    if (parent.current) {
      createPopper(parent.current, tooltip.current, {
        placement: placement ? placement : "top-end",
        modifiers: [
          {
            name: "offset",
            options: {
              offset: [0, 8],
            },
          },
        ],
      });
    }
  }, [placement, parent, tooltip]);

  useEffect(() => {
    initPopper();
  }, [initPopper]);

  const showTooltip = () => {
    if (!show) {
      setShow(true);
    }
  };
  const hideTooltip = () => {
    if (show) {
      setTimeout(() => {
        setShow(false);
      }, 250);
    }
  };
  return (
    <div ref={parent} onMouseEnter={showTooltip} onMouseLeave={hideTooltip}>
      {children}
      <div
        className={`shadow-sm border bg-white p-tooltip ${
          show ? "p-show" : "p-hidden"
        }`}
        ref={tooltip}
        role="tooltip"
      >
        {text}
        <div
          className={`p-arrow ${show ? "p-show" : "p-hidden"}`}
          data-popper-arrow
        ></div>
      </div>
    </div>
  );
}
