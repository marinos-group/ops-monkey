export default function Card({ className, children }) {
  return (
    <div className={`${className} bg-white shadow rounded-md p-2`}>
      {children}
    </div>
  );
}
