import { useEffect, useRef, useState } from "react";
import { GoLinkExternal } from "react-icons/go";
import { AiOutlineFileText } from "react-icons/ai";
import BaseTable, { AutoResizer } from "react-base-table";
import "react-base-table/styles.css";
import { Link } from "react-router-dom";
import DeploymentStatus from "./DeploymentStatus";
import WithTooltip from "../WithTooltip/";
import "./styles.css";

export default function DeploymentsTable({ title, deployments }) {
  const [box, setBox] = useState({});
  const ref = useRef(null);
  const columns = [
    {
      key: "col0",
      dataKey: "col0",
      width: 50,
      cellRenderer: ({ cellData: active }) => (
        <DeploymentStatus status={active ? "success" : "unknown"} />
      ),
    },
    {
      title: "Environment",
      key: "col1",
      dataKey: "col1",
      width: 200,
    },
    {
      title: "Control",
      key: "col2",
      dataKey: "col2",
      width: box.width - 250,
      align: "right",
      cellRenderer: ({ cellData: val }) => (
        <>
          <WithTooltip text={val}>
            <a href="https://google.com">
              <AiOutlineFileText
                style={{ fontSize: "1.45rem" }}
                className="mx-1 text-2xl"
              />
            </a>
          </WithTooltip>
          <a href="https://google.com">
            <GoLinkExternal className="mx-1 text-2xl" />
          </a>
        </>
      ),
    },
  ];
  useEffect(() => {
    setBox({
      height: 400,
      width: ref.current ? ref.current.offsetWidth : 0,
    });
  }, []);
  const mapToData = (deployment) => {
    return {
      id: `d-${deployment.id}`,
      col0: deployment.active,
      col1: deployment.environment
        ? deployment.environment.name
        : deployment.deployedOn,
      col2: `Open d-${deployment.id}`,
    };
  };

  const renderTable = () => {
    if (!deployments) {
      return null;
    }
    return (
      <div
        ref={ref}
        style={{ width: box.width, height: deployments.length * 50 }}
        className="block w-full"
      >
        <AutoResizer>
          {({ width, height }) => (
            <BaseTable
              style={{ overflow: "initial" }}
              rowHeight={50}
              data={deployments.map(mapToData)}
              columns={columns}
              width={width}
              height={height}
              headerHeight={0}
            ></BaseTable>
          )}
        </AutoResizer>
      </div>
    );
  };
  return (
    <>
      {title}
      {renderTable()}
    </>
  );
}
