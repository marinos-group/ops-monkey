import { useEffect, useState } from "react";

export default function DeploymentStatus({ status }) {
  const [shadowColor, setShadowColor] = useState("gray");
  const [color, setColor] = useState("gray");
  useEffect(() => {
    switch (status) {
      case "success":
        setShadowColor("rgb(159 233 200)");
        setColor("rgb(5, 150, 105)");
        break;
      case "error":
        setShadowColor("rgb(255 166 166)");
        setColor("rgb(220 38 38)");
        break;
      default:
        setShadowColor("rgb(200 200 200)");
        setColor("rgb(80 80 80)");
    }
  }, [status]);
  return (
    <div
      style={{
        boxShadow: `2px 2px 10px 2px ${shadowColor} inset,1px 1px 4px 0px rgb(80 80 80 / 50%)`,
        background: color,
      }}
      className=" w-4 h-4 m-auto rounded-full "
    ></div>
  );
}
