import { Routes, Route, Navigate } from "react-router-dom";
import "./App.css";
import Environment from "./pages/Environment";
import Environments from "./pages/Environments";
import HeroExample from "./pages/HeroExample";
import ManagedApplication from "./pages/ManagedApplication";
import ManagedApplications from "./pages/ManagedApplications";
import SiteMap from "./pages/SiteMap";

function App(props) {
  return (
    <>
      <Routes>
        <Route path="/" element={<SiteMap />}>
          <Route path="hero" element={<HeroExample />} />
          <Route path="apps" element={<ManagedApplications />}></Route>
          <Route path="app">
            <Route index element={<Navigate replace to="/apps" />}></Route>
            <Route path=":appId" element={<ManagedApplication />} />
          </Route>
          <Route path="environments" element={<Environments />}></Route>
          <Route path="environment">
            <Route
              index
              element={<Navigate replace to="/environment" />}
            ></Route>
            <Route path=":envId" element={<Environment />} />
          </Route>
        </Route>
      </Routes>
    </>
  );
}

export default App;
