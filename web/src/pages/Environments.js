import { Link } from "react-router-dom";
import { envs } from "../api/DummyData";

const Environments = () => {
  return envs.map((env) => {
    return (
      <div key={env.id}>
        <Link to={env.id}>{env.name}</Link>
      </div>
    );
  });
};
export default Environments;
