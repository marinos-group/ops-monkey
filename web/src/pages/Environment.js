import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { envs } from "../api/DummyData";
import Card from "../components/Card";
import DeploymentsTable from "../components/deployment/DeploymentsTable";
import Titlebar from "../components/TitleBar";

export default function Environment() {
  const [env, setEnv] = useState(null);
  const { envId } = useParams();
  useEffect(() => {
    setEnv(envs.find((item) => item.id === envId));
  }, [envId]);

  if (!env) {
    return null;
  }
  return (
    <div id="main-content" className=" w-screen max-w-4xl p-2 mx-auto">
      <Card className="mb-4">
        <Titlebar
          primaryText={env.name}
          secondaryText={
            <Link
              to={`/app/${env.managedApplicationId}`}
            >{`Back to App ${env.managedApplicationId}`}</Link>
          }
        />
      </Card>
      <div>
        <Card>
          <DeploymentsTable
            title={<Titlebar primaryText={"Active Deployments"}></Titlebar>}
            deployments={env.deployments.filter((d) => d.active)}
          ></DeploymentsTable>
        </Card>
        <Card className="mt-4">
          <DeploymentsTable
            title={<Titlebar primaryText={"Older Deployments"}></Titlebar>}
            deployments={env.deployments.filter((d) => !d.active)}
          ></DeploymentsTable>
        </Card>
      </div>
    </div>
  );
}
