import { NavLink, Outlet } from "react-router-dom";

export default function SiteMap(props) {
  const StyledLink = ({ to, children }) => {
    return (
      <div>
        <NavLink
          style={({ isActive }) => (isActive ? { color: "red" } : undefined)}
          to={to}
        >
          {children}
        </NavLink>
      </div>
    );
  };

  return (
    <>
      <Outlet />
      <div>
        <hr></hr>
        <StyledLink key="1" to="/">
          SiteMap
        </StyledLink>
        <StyledLink key="2" to="/apps">
          All Apps
        </StyledLink>
        <StyledLink key="3" to="/app/1">
          App 1
        </StyledLink>
        <StyledLink key="5" to="/environments">
          All Environments
        </StyledLink>
        <StyledLink key="6" to="/environment/1">
          Environment 1
        </StyledLink>
        <StyledLink key="4" to="/hero">
          ExampleHero
        </StyledLink>
      </div>
    </>
  );
}
