import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { apps } from "../api/DummyData";
import Card from "../components/Card";
import DeploymentsTable from "../components/deployment/DeploymentsTable";
import Titlebar from "../components/TitleBar";

const ManagedApplication = () => {
  const [app, setApp] = useState(null);
  const { appId } = useParams();
  useEffect(() => {
    setApp(apps.find((item) => item.id === appId));
  }, [appId]);

  if (!app) {
    return null;
  }
  return (
    <div id="main-content" className=" w-screen max-w-4xl p-2 mx-auto">
      <Card className="mb-4">
        <Titlebar primaryText={app.remoteProjectId} sideElement={"v1.0.1"} />
      </Card>
      <div>
        <Card>
          <DeploymentsTable
            title={<Titlebar primaryText={"Active Environments"}></Titlebar>}
            deployments={app.activeDeployments}
          ></DeploymentsTable>
        </Card>
      </div>
    </div>
  );
};

export default ManagedApplication;
