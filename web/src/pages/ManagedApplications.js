import { Link } from "react-router-dom";
import { apps } from "../api/DummyData";

const ManagedApplications = () => {
  return apps.map((app) => {
    return (
      <div key={app.id}>
        <Link to={app.id}>{app.remoteProjectId}</Link>
      </div>
    );
  });
};
export default ManagedApplications;
