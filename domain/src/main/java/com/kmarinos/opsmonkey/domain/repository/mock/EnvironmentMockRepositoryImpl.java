package com.kmarinos.opsmonkey.domain.repository.mock;

import com.kmarinos.opsmonkey.domain.model.Environment;
import com.kmarinos.opsmonkey.domain.model.ManagedApplication;
import com.kmarinos.opsmonkey.domain.repository.EnvironmentRepository;
import org.hibernate.cfg.NotYetImplementedException;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.FluentQuery;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.function.LongSupplier;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Repository
@Profile({"mock", "test"})
public class EnvironmentMockRepositoryImpl implements EnvironmentRepository {

    private final Map<Long, Environment> repository;
    private transient final LongSupplier ids;

    public EnvironmentMockRepositoryImpl(Map<Long, Environment> data) {
        this.repository = data;
        ids = new AtomicLong(repository.size())::incrementAndGet;
    }

    @Override
    public List<Environment> getEnvironmentsByManagedApplicationEquals(ManagedApplication managedApplication) {
        Assert.notNull(managedApplication,MockedData.VM_APPLICATION_IS_NULL);
        Assert.notNull(managedApplication.getId(),MockedData.VM_ID_IS_NULL);
            return repository.values().stream()
                    .filter(env -> managedApplication.equals(env.getManagedApplication()))
                    .collect(Collectors.toList());
    }

    public @NonNull
    List<Environment> findAll() {
        return new ArrayList<>(repository.values());
    }

    @Override
    public @NonNull
    List<Environment> findAll(@NonNull Sort sort) {
        throw new NotYetImplementedException();
    }

    @Override
    public @NonNull
    Page<Environment> findAll(@NonNull Pageable pageable) {
        throw new NotYetImplementedException();
    }

    @Override
    public @NonNull
    List<Environment> findAllById(@NonNull Iterable<Long> longs) {
        throw new NotYetImplementedException();
    }

    @Override
    public long count() {
        return repository.size();
    }

    @Override
    public void deleteById(@NonNull Long aLong) {
        repository.remove(aLong);
    }

    @Override
    public void delete(@NonNull Environment entity) {
        repository.remove(entity.getId());
    }

    @Override
    public void deleteAllById(@NonNull Iterable<? extends Long> longs) {
        throw new NotYetImplementedException();
    }

    @Override
    public void deleteAll(@NonNull Iterable<? extends Environment> entities) {
        entities.forEach(entity -> repository.remove(entity.getId()));
    }

    @Override
    public void deleteAll() {
        throw new NotYetImplementedException();
    }

    @Override
    public @NonNull
    <S extends Environment> S save(@NonNull S entity) {
        if (entity.getId() == null) {
            entity.setId(ids.getAsLong());
        }
        repository.put(entity.getId(), entity);
        return entity;
    }

    @Override
    public @NonNull
    <S extends Environment> List<S> saveAll(@NonNull Iterable<S> entities) {
        return StreamSupport.stream(
                        Spliterators.spliteratorUnknownSize(
                                entities.iterator(), Spliterator.ORDERED), false)
                .map(this::save).collect(Collectors.toList());
    }

    @Override
    public @NonNull
    Optional<Environment> findById(@NonNull Long aLong) {
        return Optional.ofNullable(repository.get(aLong));
    }

    @Override
    public boolean existsById(@NonNull Long aLong) {
        return repository.containsKey(aLong);
    }

    @Override
    public void flush() {
        throw new NotYetImplementedException();
    }

    @Override
    public @NonNull
    <S extends Environment> S saveAndFlush(@NonNull S entity) {
        return this.save(entity);
    }

    @Override
    public @NonNull
    <S extends Environment> List<S> saveAllAndFlush(@NonNull Iterable<S> entities) {
        return this.saveAll(entities);
    }

    @Override
    public void deleteAllInBatch(@NonNull Iterable<Environment> entities) {
        StreamSupport.stream(
                Spliterators.spliteratorUnknownSize(
                        entities.iterator(), Spliterator.ORDERED), false).forEach(this::delete);
    }

    @Override
    @SuppressWarnings("all")
    public void deleteAllByIdInBatch(@NonNull Iterable<Long> longs) {
        throw new NotYetImplementedException();
    }

    @Override
    public void deleteAllInBatch() {
        throw new NotYetImplementedException();
    }

    @Override
    public @NonNull
    Environment getOne(@NonNull Long aLong) {
        throw new NotYetImplementedException();
    }

    @Override
    public @NonNull
    Environment getById(@NonNull Long aLong) {
        return repository.get(aLong);
    }

    @Override
    public @NonNull
    <S extends Environment> Optional<S> findOne(@NonNull Example<S> example) {
        throw new NotYetImplementedException();
    }

    @Override
    public @NonNull
    <S extends Environment> List<S> findAll(@NonNull Example<S> example) {
        throw new NotYetImplementedException();
    }

    @Override
    public @NonNull
    <S extends Environment> List<S> findAll(@NonNull Example<S> example, @NonNull Sort sort) {
        throw new NotYetImplementedException();
    }

    @Override
    public @NonNull
    <S extends Environment> Page<S> findAll(@NonNull Example<S> example, @NonNull Pageable pageable) {
        throw new NotYetImplementedException();
    }

    @Override
    public @NonNull
    <S extends Environment> long count(@NonNull Example<S> example) {
        throw new NotYetImplementedException();
    }

    @Override
    public @NonNull
    <S extends Environment> boolean exists(@NonNull Example<S> example) {
        throw new NotYetImplementedException();
    }

    @Override
    public @NonNull
    <S extends Environment, R> R findBy(@NonNull Example<S> example, @NonNull Function<FluentQuery.FetchableFluentQuery<S>, R> queryFunction) {
        throw new NotYetImplementedException();
    }


}
