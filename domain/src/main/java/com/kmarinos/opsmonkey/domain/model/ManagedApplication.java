package com.kmarinos.opsmonkey.domain.model;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ManagedApplication {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private Long id;

    private String remoteProjectId;
    @Transient
    @Builder.Default
    @ToString.Exclude
    Set<Deployment> activeDeployments = new HashSet<>();

}
