package com.kmarinos.opsmonkey.domain.repository;

import com.kmarinos.opsmonkey.domain.model.Environment;
import com.kmarinos.opsmonkey.domain.model.ManagedApplication;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EnvironmentRepository extends JpaRepository<Environment,Long> {

    List<Environment> getEnvironmentsByManagedApplicationEquals(ManagedApplication managedApplication);
}
