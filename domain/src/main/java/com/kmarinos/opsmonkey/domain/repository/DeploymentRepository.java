package com.kmarinos.opsmonkey.domain.repository;

import com.kmarinos.opsmonkey.domain.model.Deployment;
import com.kmarinos.opsmonkey.domain.model.Environment;
import com.kmarinos.opsmonkey.domain.model.ManagedApplication;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DeploymentRepository extends JpaRepository<Deployment,Long> {

    List<Deployment> findByActiveTrueAndManagedApplicationEquals(ManagedApplication managedApplication);
    List<Deployment> getAllByEnvironment(Environment environment);

}
