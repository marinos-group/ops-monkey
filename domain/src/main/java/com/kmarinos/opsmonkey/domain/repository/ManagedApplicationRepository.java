package com.kmarinos.opsmonkey.domain.repository;

import com.kmarinos.opsmonkey.domain.model.ManagedApplication;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ManagedApplicationRepository extends JpaRepository<ManagedApplication,Long> {

    Optional<ManagedApplication> findManagedApplicationByRemoteProjectIdEquals(String remoteProjectId);
}
