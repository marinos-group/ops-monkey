package com.kmarinos.opsmonkey.domain.repository.mock;

import com.kmarinos.opsmonkey.domain.model.Deployment;
import com.kmarinos.opsmonkey.domain.model.Environment;
import com.kmarinos.opsmonkey.domain.model.ManagedApplication;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.IntSupplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
@Profile("mock")
public class MockedData {

    Map<Long, ManagedApplication> applicationMap;
    Map<Long, Environment> environmentMap;
    Map<Long, Deployment> deploymentMap;

    transient IntSupplier environmentId = new AtomicInteger(0)::incrementAndGet;
    transient IntSupplier deploymentId = new AtomicInteger(0)::incrementAndGet;

    public static final  String VM_APPLICATION_IS_NULL ="ManagedApplication cannot be null";
    public static final  String VM_ID_IS_NULL ="Id cannot be null";
    public static final String VM_ENVIRONMENT_IS_NULL ="Environment cannot be null";

    public MockedData() {
        init();
    }

    public void init() {
        applicationMap = new HashMap<>();
        environmentMap = new HashMap<>();
        deploymentMap = new HashMap<>();

        this.generateData(5, 2, 3);
        //Deployments reference all other entities, so we traverse the map
        //to fix the references to application and environment
        deploymentMap.values().forEach(deployment -> {
            ManagedApplication app = deployment.getManagedApplication();
            Environment env = deployment.getEnvironment();
            env.getDeployments().add(deployment);
            if (deployment.isActive()) {
                app.getActiveDeployments().add(deployment);
            }
        });

    }

    @Bean
    @Qualifier("mocked-applications")
    public Map<Long, ManagedApplication> getMockApplicationData() {
        return applicationMap;
    }

    @Bean
    @Qualifier("mocked-environments")
    public Map<Long, Environment> getMockEnvironmentData() {
        return environmentMap;
    }

    @Bean
    @Qualifier("mocked-deployments")
    public Map<Long, Deployment> getMockDeploymentData() {
        return deploymentMap;
    }

    private void generateData(int totalApplications, int maxEnvironments, int maxDeployments) {
        IntStream.range(1, totalApplications + 1).mapToObj(this::generateApplicationWithId)
                .flatMap(app -> {
                    applicationMap.put(app.getId(),app);
                    return this.generateEnvironments(app,
                            ThreadLocalRandom.current().nextInt(0, maxEnvironments + 1))
                            .stream();
                })
                .flatMap(env ->{
                    environmentMap.put(env.getId(),env);
                    return generateDeployments(env.getManagedApplication(),
                            env,
                            ThreadLocalRandom.current().nextInt(0, maxDeployments + 1))
                            .stream();})
                .forEach(deployment -> deploymentMap.put(deployment.getId(), deployment));
    }

    private ManagedApplication generateApplicationWithId(int id) {
        return ManagedApplication.builder()
                .id((long) id)
                .remoteProjectId("Project-" + id)
                .build();
    }

    private Environment generateEnvironmentWithId(final ManagedApplication app, int id) {
        return Environment.builder()
                .id((long)id)
                .name("env-" + id)
                .managedApplication(app)
                .build();
    }

    private List<Environment> generateEnvironments(final ManagedApplication app, int total) {
        return IntStream.range(1, total + 1).mapToObj(idx -> this.generateEnvironmentWithId(app, environmentId.getAsInt())).collect(Collectors.toList());
    }

    private Set<Deployment> generateDeployments(final ManagedApplication app, final Environment env, int total) {
        return IntStream.range(1, total + 1).mapToObj(idx -> this.generateDeploymentWithId(app, env, deploymentId.getAsInt())).collect(Collectors.toSet());
    }


    private Deployment generateDeploymentWithId(final ManagedApplication app, final Environment env, int id) {
        final var deployment = Deployment.builder()
                .id((long) id)
                .referenceLink("https://reference/" + id)
                .managedApplication(app)
                .environment(env)
                .build();
        if (ThreadLocalRandom.current().nextBoolean()) {
            deployment.deploy("https://deployed/" + id);
        }
        return deployment;
    }

}
