package com.kmarinos.opsmonkey.domain.model;

import com.fasterxml.jackson.annotation.*;
import lombok.*;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Deployment {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private Long id;
    @ManyToOne
    @JsonIdentityReference(alwaysAsId = true)
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "id")
    @JsonProperty("managedApplicationId")
    @ToString.Exclude
    private ManagedApplication managedApplication;
    @ManyToOne
    @JsonIdentityReference(alwaysAsId = true)
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "id")
    @JsonProperty("environmentId")
    @ToString.Exclude
    private Environment environment;

    String referenceLink;
    @Setter(AccessLevel.PRIVATE)
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    String deployedOn;
    @Setter(AccessLevel.PRIVATE)
    boolean active;

    @Builder
    public Deployment(Long id,ManagedApplication managedApplication,Environment environment,String referenceLink,String deployedOn){
        this.id=id;
        this.managedApplication=managedApplication;
        this.environment=environment;
        this.referenceLink=referenceLink;
        deploy(deployedOn,false);
    }

    public final void deploy(String deployedOn,boolean force){
        if(deployedOn==null){
            return;
        }
        if(force){
            undeploy();
        }
        if(this.deployedOn!=null){
            throw new IllegalStateException("The deployment is already active. Call undeploy first.");
        }
        this.active=true;
        this.deployedOn=deployedOn;
    }
    public void deploy(String deployedOn){
        deploy(deployedOn,false);
    }
    public void undeploy(){
        this.active=false;
        this.deployedOn=null;
    }

}




