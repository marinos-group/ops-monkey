package com.kmarinos.opsmonkey.domain.repository.mock;

import com.kmarinos.opsmonkey.domain.model.Deployment;
import com.kmarinos.opsmonkey.domain.model.Environment;
import com.kmarinos.opsmonkey.domain.model.ManagedApplication;
import com.kmarinos.opsmonkey.domain.repository.DeploymentRepository;
import org.hibernate.cfg.NotYetImplementedException;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.FluentQuery;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.function.LongSupplier;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.kmarinos.opsmonkey.domain.repository.mock.MockedData.VM_ID_IS_NULL;
import static com.kmarinos.opsmonkey.domain.repository.mock.MockedData.VM_APPLICATION_IS_NULL;
import static com.kmarinos.opsmonkey.domain.repository.mock.MockedData.VM_ENVIRONMENT_IS_NULL;

@Repository
@Profile({"mock", "test"})
public class DeploymentMockRepositoryImpl implements DeploymentRepository {

    private final Map<Long, Deployment> repository;
    private transient final LongSupplier ids;


    public DeploymentMockRepositoryImpl(Map<Long, Deployment> data) {
        this.repository = data;
        ids = new AtomicLong(repository.size())::incrementAndGet;
    }

    @Override
    public List<Deployment> findByActiveTrueAndManagedApplicationEquals(ManagedApplication managedApplication) {
        Assert.notNull(managedApplication, VM_APPLICATION_IS_NULL);
        Assert.notNull(managedApplication.getId(), VM_ID_IS_NULL);
        return repository.values().stream()
                .filter(Deployment::isActive)
                .filter(deployment -> managedApplication.equals(deployment.getManagedApplication()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Deployment> getAllByEnvironment(Environment environment) {
        Assert.notNull(environment, VM_ENVIRONMENT_IS_NULL);
        Assert.notNull(environment.getId(), VM_ID_IS_NULL);
        return repository.values().stream()
                .filter(deployment -> environment.equals(deployment.getEnvironment()))
                .collect(Collectors.toList());
    }

    public @NonNull
    List<Deployment> findAll() {
        return new ArrayList<>(repository.values());
    }

    @Override
    public @NonNull
    List<Deployment> findAll(@NonNull Sort sort) {
        throw new NotYetImplementedException();
    }

    @Override
    public @NonNull
    Page<Deployment> findAll(@NonNull Pageable pageable) {
        throw new NotYetImplementedException();
    }

    @Override
    public @NonNull
    List<Deployment> findAllById(@NonNull Iterable<Long> longs) {
        throw new NotYetImplementedException();
    }

    @Override
    public long count() {
        return repository.size();
    }

    @Override
    public void deleteById(@NonNull Long aLong) {
        repository.remove(aLong);
    }

    @Override
    public void delete(@NonNull Deployment entity) {
        repository.remove(entity.getId());
    }

    @Override
    public void deleteAllById(@NonNull Iterable<? extends Long> longs) {
        throw new NotYetImplementedException();
    }

    @Override
    public void deleteAll(@NonNull Iterable<? extends Deployment> entities) {
        entities.forEach(entity -> repository.remove(entity.getId()));
    }

    @Override
    public void deleteAll() {
        throw new NotYetImplementedException();
    }

    @Override
    public @NonNull
    <S extends Deployment> S save(@NonNull S entity) {
        if (entity.getId() == null) {
            entity.setId(ids.getAsLong());
        }
        repository.put(entity.getId(), entity);
        return entity;
    }

    @Override
    public @NonNull
    <S extends Deployment> List<S> saveAll(@NonNull Iterable<S> entities) {
        return StreamSupport.stream(
                        Spliterators.spliteratorUnknownSize(
                                entities.iterator(), Spliterator.ORDERED), false)
                .map(this::save).collect(Collectors.toList());
    }

    @Override
    public @NonNull
    Optional<Deployment> findById(@NonNull Long aLong) {
        return Optional.ofNullable(repository.get(aLong));
    }

    @Override
    public boolean existsById(@NonNull Long aLong) {
        return repository.containsKey(aLong);
    }

    @Override
    public void flush() {
        throw new NotYetImplementedException();
    }

    @Override
    public @NonNull
    <S extends Deployment> S saveAndFlush(@NonNull S entity) {
        return this.save(entity);
    }

    @Override
    public @NonNull
    <S extends Deployment> List<S> saveAllAndFlush(@NonNull Iterable<S> entities) {
        return this.saveAll(entities);
    }

    @Override
    public void deleteAllInBatch(@NonNull Iterable<Deployment> entities) {
        StreamSupport.stream(
                Spliterators.spliteratorUnknownSize(
                        entities.iterator(), Spliterator.ORDERED), false).forEach(this::delete);
    }

    @Override
    @SuppressWarnings("all")
    public void deleteAllByIdInBatch(@NonNull Iterable<Long> longs) {
        throw new NotYetImplementedException();
    }

    @Override
    public void deleteAllInBatch() {
        throw new NotYetImplementedException();
    }

    @Override
    public @NonNull
    Deployment getOne(@NonNull Long aLong) {
        throw new NotYetImplementedException();
    }

    @Override
    public @NonNull
    Deployment getById(@NonNull Long aLong) {
        return repository.get(aLong);
    }

    @Override
    public @NonNull
    <S extends Deployment> Optional<S> findOne(@NonNull Example<S> example) {
        throw new NotYetImplementedException();
    }

    @Override
    public @NonNull
    <S extends Deployment> List<S> findAll(@NonNull Example<S> example) {
        throw new NotYetImplementedException();
    }

    @Override
    public @NonNull
    <S extends Deployment> List<S> findAll(@NonNull Example<S> example, @NonNull Sort sort) {
        throw new NotYetImplementedException();
    }

    @Override
    public @NonNull
    <S extends Deployment> Page<S> findAll(@NonNull Example<S> example, @NonNull Pageable pageable) {
        throw new NotYetImplementedException();
    }

    @Override
    public @NonNull
    <S extends Deployment> long count(@NonNull Example<S> example) {
        throw new NotYetImplementedException();
    }

    @Override
    public @NonNull
    <S extends Deployment> boolean exists(@NonNull Example<S> example) {
        throw new NotYetImplementedException();
    }

    @Override
    public @NonNull
    <S extends Deployment, R> R findBy(@NonNull Example<S> example, @NonNull Function<FluentQuery.FetchableFluentQuery<S>, R> queryFunction) {
        throw new NotYetImplementedException();
    }
}
