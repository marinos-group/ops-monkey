package com.kmarinos.opsmonkey.domain.deployment;

import com.kmarinos.opsmonkey.domain.model.Deployment;
import com.kmarinos.opsmonkey.domain.repository.mock.DeploymentMockRepositoryImpl;
import com.kmarinos.opsmonkey.domain.repository.mock.MockedData;
import io.github.artsok.RepeatedIfExceptionsTest;
import org.hibernate.cfg.NotYetImplementedException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class DeploymentMockRepositoryTests {

    transient MockedData mockedData;
    transient Map<Long,Deployment> repository;
    transient DeploymentMockRepositoryImpl underTest;

    @BeforeEach
    void initTest(){
        mockedData=new MockedData();
        repository=mockedData.getMockDeploymentData();
        underTest=new DeploymentMockRepositoryImpl(repository);
    }

    @RepeatedIfExceptionsTest(repeats=3)
    void given_application_should_return_active_deployments(){
        final var searchWith=repository.values().iterator().next().getManagedApplication();
        final var toTest = repository.values().stream()
                .filter(deployment -> deployment.getManagedApplication().equals(searchWith))
                .filter(Deployment::isActive)
                .collect(Collectors.toList());

        assertThat(underTest.findByActiveTrueAndManagedApplicationEquals(searchWith))
                .containsExactlyElementsOf(toTest);
    }

    @RepeatedIfExceptionsTest(repeats=3)
    void given_environment_should_return_deployments(){
        final var searchWith=repository.values().iterator().next().getEnvironment();
        final var toTest = repository.values().stream()
                .filter(deployment -> deployment.getEnvironment().equals(searchWith))
                .collect(Collectors.toList());

        assertThat(underTest.getAllByEnvironment(searchWith))
                .containsExactlyElementsOf(toTest);
    }

    @RepeatedIfExceptionsTest(repeats=3)
    void given_nothing_should_return_all_deployments(){
        assertThat(underTest.findAll()).containsAll(repository.values());
    }
    @RepeatedIfExceptionsTest(repeats=3)
    void given_sort_in_findAll_should_throw_nyi_exception(){
        assertThrows(NotYetImplementedException.class,()->underTest.findAll(Sort.unsorted()));
    }
    @RepeatedIfExceptionsTest(repeats=3)
    void given_pageable_in_findAll_should_throw_nyi_exception(){
        assertThrows(NotYetImplementedException.class,()->underTest.findAll(Pageable.unpaged()));
    }
    @RepeatedIfExceptionsTest(repeats=3)
    void given_ids_in_findAllById_should_throw_nyi_exception(){
        assertThrows(NotYetImplementedException.class,()->underTest.findAllById(new ArrayList<>()));
    }
    @RepeatedIfExceptionsTest(repeats=3)
    void given_nothing_should_return_total_entries(){
        assertThat(underTest.count()).isEqualTo(repository.size());
    }

    @RepeatedIfExceptionsTest(repeats=3)
    void given_id_should_delete_deployment(){
        final var id=repository.values().iterator().next().getId();

        underTest.deleteById(id);

        assertThat(repository).doesNotContainKey(id);
    }
    @RepeatedIfExceptionsTest(repeats=3)
    void given_deployment_should_delete_deployment(){
        final var toDelete=repository.values().iterator().next();

        underTest.delete(toDelete);

        assertThat(repository).doesNotContainValue(toDelete);
    }
    @RepeatedIfExceptionsTest(repeats=3)
    void given_nothing_in_deleteAllById_should_throw_nyi_exception(){
        assertThrows(NotYetImplementedException.class,()->underTest.deleteAllById(new ArrayList<>()));
    }
    @RepeatedIfExceptionsTest(repeats=3)
    void given_deployments_should_delete_deployments(){
        final var iterator = repository.values().iterator();
        final var toDelete1= iterator.next();
        final var toDelete2= iterator.next();

        underTest.deleteAll(Arrays.asList(toDelete1,toDelete2));

        assertThat(repository).doesNotContainValue(toDelete1);
        assertThat(repository).doesNotContainValue(toDelete2);
    }
    @RepeatedIfExceptionsTest(repeats=3)
    void given_nothing_in_deleteAll_should_throw_nyi_exception(){
        assertThrows(NotYetImplementedException.class,()->underTest.deleteAll());
    }
    @RepeatedIfExceptionsTest(repeats=3)
    void given_deployment_should_save_deployment(){
        final var toSave = Deployment.builder().build();
        int totalEntities = repository.size();

        underTest.save(toSave);

        assertThat(repository).containsValue(toSave);
        assertThat(repository.size()).isEqualTo(totalEntities+1);
    }
    @RepeatedIfExceptionsTest(repeats=3)
    void given_deployments_should_save_deployments(){
        final var toSave1 = Deployment.builder().build();
        final var toSave2 = Deployment.builder().build();
        int totalEntities = repository.size();
        underTest.saveAll(Arrays.asList(toSave1,toSave2));

        assertThat(repository).containsValues(toSave1,toSave2);
        assertThat(repository.size()).isEqualTo(totalEntities+2);
    }
    @RepeatedIfExceptionsTest(repeats=3)
    void given_id_should_return_deployment(){
        final var toFind = repository.values().iterator().next();
        final var id= toFind.getId();
        assertThat(underTest.findById(id)).hasValue(toFind);
    }
    @RepeatedIfExceptionsTest(repeats=3)
    void given_missing_id_should_return_empty(){
        final var missingId=-1L;
        assertThat(underTest.findById(missingId)).isEmpty();
    }
    @RepeatedIfExceptionsTest(repeats=3)
    void given_existing_id_should_return_true(){
        final var id = repository.keySet().iterator().next();
        assertThat(underTest.existsById(id)).isTrue();
    }
    @RepeatedIfExceptionsTest(repeats=3)
    void given_nothing_in_flush_should_throw_nyi_exception(){
        assertThrows(NotYetImplementedException.class,()->underTest.flush());
    }
    @RepeatedIfExceptionsTest(repeats=3)
    void given_deployment_should_save_deployment_in_saveAndFlush(){
        final var toSave = Deployment.builder().build();
        int totalEntities = repository.size();

        underTest.saveAndFlush(toSave);

        assertThat(repository).containsValue(toSave);
        assertThat(repository.size()).isEqualTo(totalEntities+1);
    }
    @RepeatedIfExceptionsTest(repeats=3)
    void given_deployments_should_save_deployments_in_saveAllAndFlush(){
        final var toSave1 = Deployment.builder().build();
        final var toSave2 = Deployment.builder().build();
        int totalEntities = repository.size();

        underTest.saveAllAndFlush(Arrays.asList(toSave1,toSave2));

        assertThat(repository).containsValues(toSave1,toSave2);
        assertThat(repository.size()).isEqualTo(totalEntities+2);
    }
    @RepeatedIfExceptionsTest(repeats=3)
    void given_deployments_should_delete_deployments_in_deleteAllInBatch(){
        final var iterator = repository.values().iterator();
        final var toDelete1= iterator.next();
        final var toDelete2= iterator.next();

        underTest.deleteAllInBatch(Arrays.asList(toDelete1,toDelete2));

        assertThat(repository).doesNotContainValue(toDelete1);
        assertThat(repository).doesNotContainValue(toDelete2);
    }
    @RepeatedIfExceptionsTest(repeats=3)
    void given_ids_in_deleteAllByIdInBatch_should_throw_nyi_exception(){
        assertThrows(NotYetImplementedException.class,()->underTest.deleteAllByIdInBatch(new ArrayList<>()));
    }
    @RepeatedIfExceptionsTest(repeats=3)
    void given_nothing_in_deleteAllInBatch_should_throw_nyi_exception(){
        assertThrows(NotYetImplementedException.class,()->underTest.deleteAllInBatch());
    }
    @RepeatedIfExceptionsTest(repeats=3)
    void given_id_in_getOne_should_throw_nyi_exception(){
        assertThrows(NotYetImplementedException.class,()->underTest.getOne(1L));
    }
    @RepeatedIfExceptionsTest(repeats=3)
    void given_id_should_return_deployment_in_getById(){
        final var toFind = repository.values().iterator().next();
        final var existingId= toFind.getId();
        assertThat(underTest.getById(existingId)).isEqualTo(toFind);
    }
    @RepeatedIfExceptionsTest(repeats=3)
    void given_example_deployment_in_findOne_should_throw_nyi_exception(){
        assertThrows(NotYetImplementedException.class,()->underTest.findOne(Example.of(Deployment.builder().build())));
    }
    @RepeatedIfExceptionsTest(repeats=3)
    void given_example_deployment_in_findAll_should_throw_nyi_exception(){
        assertThrows(NotYetImplementedException.class,()->underTest.findAll(Example.of(Deployment.builder().build())));
    }
    @RepeatedIfExceptionsTest(repeats=3)
    void given_example_deployment_and_sort_in_findAll_should_throw_nyi_exception(){
        assertThrows(NotYetImplementedException.class,
                ()->underTest.findAll(Example.of(Deployment.builder().build()),Sort.unsorted()));
    }
    @RepeatedIfExceptionsTest(repeats=3)
    void given_example_deployment_and_pageable_in_findAll_should_throw_nyi_exception(){
        assertThrows(NotYetImplementedException.class,
                ()->underTest.findAll(Example.of(Deployment.builder().build()),Pageable.unpaged()));
    }
    @RepeatedIfExceptionsTest(repeats=3)
    void given_example_deployment_in_count_should_throw_nyi_exception(){
        assertThrows(NotYetImplementedException.class,
                ()->underTest.count(Example.of(Deployment.builder().build())));
    }
    @RepeatedIfExceptionsTest(repeats=3)
    void given_example_deployment_in_exists_should_throw_nyi_exception(){
        assertThrows(NotYetImplementedException.class,
                ()->underTest.exists(Example.of(Deployment.builder().build())));
    }
    @RepeatedIfExceptionsTest(repeats=3)
    void given_example_deployment_and_query_in_findBy_should_throw_nyi_exception(){
        assertThrows(NotYetImplementedException.class,
                ()->underTest.findBy(Example.of(Deployment.builder().build()),
                        Function.identity()));
    }

}
