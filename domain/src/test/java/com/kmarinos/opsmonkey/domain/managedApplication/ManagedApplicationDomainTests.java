package com.kmarinos.opsmonkey.domain.managedApplication;

import com.kmarinos.opsmonkey.domain.model.Deployment;
import com.kmarinos.opsmonkey.domain.model.ManagedApplication;
import  static org.assertj.core.api.Assertions.*;
import org.junit.jupiter.api.Test;

import java.util.HashSet;

class ManagedApplicationDomainTests {
    @Test
    void constructorShouldCreateValidManagedApplication(){
        final var id = 1L;
        final var remoteProjectId = "Project-1";
        final var deployments = new HashSet<Deployment>();

        final var app = new ManagedApplication(id, remoteProjectId, deployments);

        assertThat(app.getId()).isEqualTo(id);
        assertThat(app.getRemoteProjectId()).isEqualTo(remoteProjectId);
        assertThat(app.getActiveDeployments()).isEqualTo(deployments);
    }
    @Test void builderShouldCreateValidManagedApplication(){
        final var id = 1L;
        final var remoteProjectId = "Project-1";

        final var app = ManagedApplication.builder()
                .id(id)
                .remoteProjectId(remoteProjectId)
                .build();

        assertThat(app.getId()).isEqualTo(id);
        assertThat(app.getRemoteProjectId()).isEqualTo(remoteProjectId);
        assertThat(app.getActiveDeployments()).isEqualTo(new HashSet<>());
    }
}
