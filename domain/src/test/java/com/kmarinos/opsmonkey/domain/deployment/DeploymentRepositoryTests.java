package com.kmarinos.opsmonkey.domain.deployment;

import com.kmarinos.opsmonkey.domain.model.Deployment;
import com.kmarinos.opsmonkey.domain.model.ManagedApplication;
import com.kmarinos.opsmonkey.domain.repository.DeploymentRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@ContextConfiguration(classes= DeploymentRepositoryTests.class)
@EnableJpaRepositories(basePackages = "com.kmarinos.*")
@EntityScan("com.kmarinos.*")
class DeploymentRepositoryTests {
    @Autowired
    private transient TestEntityManager testEntityManager;
    @Autowired
    private transient DeploymentRepository deploymentRepository;

    @Test
    void given_application_should_return_all_active_deployments(){
        final var app = ManagedApplication.builder()
                .remoteProjectId("Project-1")
                .build();
        testEntityManager.persist(app);

        final var deployment1 = Deployment.builder()
                .managedApplication(app)
                .deployedOn("https://deployed")
                .build();
        final var deployment2 = Deployment.builder()
                .managedApplication(app)
                .deployedOn("https://deployed")
                .build();
        final var deployment3 = Deployment.builder()
                .deployedOn("https://deployed")
                .build();
        final var deployment4 = Deployment.builder()
                .managedApplication(app)
                .build();
        testEntityManager.persist(deployment1);
        testEntityManager.persist(deployment2);
        testEntityManager.persist(deployment3);
        testEntityManager.persist(deployment4);

        assertThat(deploymentRepository.findByActiveTrueAndManagedApplicationEquals(app))
                .containsExactly(deployment1,deployment2);
    }
}
