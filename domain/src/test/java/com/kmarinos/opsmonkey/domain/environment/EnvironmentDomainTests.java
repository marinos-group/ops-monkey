package com.kmarinos.opsmonkey.domain.environment;

import com.kmarinos.opsmonkey.domain.model.Deployment;
import com.kmarinos.opsmonkey.domain.model.Environment;
import com.kmarinos.opsmonkey.domain.model.ManagedApplication;
import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static org.assertj.core.api.Assertions.assertThat;

class EnvironmentDomainTests {
    @Test
    void constructorShouldCreateValidEnvironment(){
        final var id=1L;
        final var name="env-1";
        final var app = new ManagedApplication();
        final var deployments = new HashSet<Deployment>();

        final var environment = new Environment(id,name,app,deployments);

        assertThat(environment.getId()).isEqualTo(id);
        assertThat(environment.getName()).isEqualTo(name);
        assertThat(environment.getManagedApplication()).isNotNull();
        assertThat(environment.getManagedApplication()).isInstanceOf(ManagedApplication.class);
        assertThat(environment.getDeployments()).isEqualTo(deployments);
    }
    @Test
    void builderShouldCreateValidEnvironment(){
        final var id=1L;
        final var name="env-1";
        final var app = new ManagedApplication();

        final var environment = Environment.builder()
                .id(id)
                .name(name)
                .managedApplication(app)
                .build();

        assertThat(environment.getId()).isEqualTo(id);
        assertThat(environment.getName()).isEqualTo(name);
        assertThat(environment.getManagedApplication()).isNotNull();
        assertThat(environment.getManagedApplication()).isInstanceOf(ManagedApplication.class);
        assertThat(environment.getDeployments()).isEqualTo(new HashSet<>());
    }
}
