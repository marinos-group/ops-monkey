package com.kmarinos.opsmonkey.domain.managedApplication;

import com.kmarinos.opsmonkey.domain.model.ManagedApplication;
import com.kmarinos.opsmonkey.domain.repository.ManagedApplicationRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@ContextConfiguration(classes=ManagedApplicationRepositoryTests.class)
@EnableJpaRepositories(basePackages = "com.kmarinos.*")
@EntityScan("com.kmarinos.*")
class ManagedApplicationRepositoryTests {

    @Autowired
    private transient TestEntityManager testEntityManager;
    @Autowired
    private transient ManagedApplicationRepository managedApplicationRepository;

    @Test
    void given_remote_project_id_should_return_application(){
        final var remoteProjectId1 = "Project-1";
        final var remoteProjectId2 = "Project-2";

        final var app1 = new ManagedApplication(null, remoteProjectId1, null);
        final var app2 = new ManagedApplication(null, remoteProjectId2, null);
        testEntityManager.persist(app1);
        testEntityManager.persist(app2);

        assertThat(managedApplicationRepository
                .findManagedApplicationByRemoteProjectIdEquals("Project-1").orElseThrow()).isEqualTo(app1);
    }
}
