package com.kmarinos.opsmonkey.domain.environment;

import com.kmarinos.opsmonkey.domain.model.Environment;
import com.kmarinos.opsmonkey.domain.model.ManagedApplication;
import com.kmarinos.opsmonkey.domain.repository.EnvironmentRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@ContextConfiguration(classes= EnvironmentRepositoryTests.class)
@EnableJpaRepositories(basePackages = "com.kmarinos.*")
@EntityScan("com.kmarinos.*")
class EnvironmentRepositoryTests {
    @Autowired
    private transient TestEntityManager testEntityManager;
    @Autowired
    private transient EnvironmentRepository environmentRepository;

    @Test
    void given_application_should_return_all_environments(){
        final var app = ManagedApplication.builder()
                .remoteProjectId("Project-1")
                .build();
        testEntityManager.persist(app);

        final var env1 = Environment.builder()
                .name("env-1")
                .managedApplication(app)
                .build();
        final var env2 = Environment.builder()
                .name("env-2")
                .managedApplication(app)
                .build();
        final var env3 = Environment.builder()
                .name("env-3")
                .build();
        testEntityManager.persist(env1);
        testEntityManager.persist(env2);
        testEntityManager.persist(env3);

        assertThat(environmentRepository.getEnvironmentsByManagedApplicationEquals(app))
                .containsExactly(env1,env2);
    }
}
