package com.kmarinos.opsmonkey.domain.deployment;

import com.kmarinos.opsmonkey.domain.model.Deployment;
import com.kmarinos.opsmonkey.domain.model.Environment;
import com.kmarinos.opsmonkey.domain.model.ManagedApplication;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class DeploymentDomainTests {
    @Test
    void constructorShouldCreateValidActiveDeployment() {
        final var id = 1L;
        final var app = new ManagedApplication();
        final var env = new Environment();
        final var referenceLink = "https://reference";
        final var deployedOn = "https://deployed";

        final var deployment = new Deployment(id, app, env, referenceLink, deployedOn);

        assertThat(deployment.getId()).isEqualTo(id);
        assertThat(deployment.getManagedApplication()).isNotNull();
        assertThat(deployment.getManagedApplication()).isInstanceOf(ManagedApplication.class);
        assertThat(deployment.getEnvironment()).isNotNull();
        assertThat(deployment.getEnvironment()).isInstanceOf(Environment.class);
        assertThat(deployment.getReferenceLink()).isEqualTo(referenceLink);
        assertThat(deployment.getDeployedOn()).isEqualTo(deployedOn);
        assertThat(deployment.isActive()).isEqualTo(true);

    }
    @Test
    void constructorShouldCreateValidInactiveDeployment() {
        final var id = 1L;
        final var app = new ManagedApplication();
        final var env = new Environment();
        final var referenceLink = "https://reference";

        final var deployment = new Deployment(id, app, env, referenceLink, null);

        assertThat(deployment.getId()).isEqualTo(id);
        assertThat(deployment.getManagedApplication()).isNotNull();
        assertThat(deployment.getManagedApplication()).isInstanceOf(ManagedApplication.class);
        assertThat(deployment.getEnvironment()).isNotNull();
        assertThat(deployment.getEnvironment()).isInstanceOf(Environment.class);
        assertThat(deployment.getReferenceLink()).isEqualTo(referenceLink);
        assertThat(deployment.getDeployedOn()).isNull();
        assertThat(deployment.isActive()).isEqualTo(false);

    }

    @Test
    void builderShouldCreateValidActiveDeployment() {
        final var id = 1L;
        final var app = new ManagedApplication();
        final var env = new Environment();
        final var referenceLink = "https://reference";
        final var deployedOn = "https://deployed";

        final var deployment = Deployment.builder()
                .id(id)
                .managedApplication(app)
                .environment(env)
                .referenceLink(referenceLink)
                .deployedOn(deployedOn)
                .build();

        assertThat(deployment.getId()).isEqualTo(id);
        assertThat(deployment.getManagedApplication()).isNotNull();
        assertThat(deployment.getManagedApplication()).isInstanceOf(ManagedApplication.class);
        assertThat(deployment.getEnvironment()).isNotNull();
        assertThat(deployment.getEnvironment()).isInstanceOf(Environment.class);
        assertThat(deployment.getReferenceLink()).isEqualTo(referenceLink);
        assertThat(deployment.getDeployedOn()).isEqualTo(deployedOn);
        assertThat(deployment.isActive()).isEqualTo(true);
    }
    @Test
    void builderShouldCreateValidInactiveDeployment() {
        final var id = 1L;
        final var app = new ManagedApplication();
        final var env = new Environment();
        final var referenceLink = "https://reference";

        final var deployment = Deployment.builder()
                .id(id)
                .managedApplication(app)
                .environment(env)
                .referenceLink(referenceLink)
                .build();

        assertThat(deployment.getId()).isEqualTo(id);
        assertThat(deployment.getManagedApplication()).isNotNull();
        assertThat(deployment.getManagedApplication()).isInstanceOf(ManagedApplication.class);
        assertThat(deployment.getEnvironment()).isNotNull();
        assertThat(deployment.getEnvironment()).isInstanceOf(Environment.class);
        assertThat(deployment.getReferenceLink()).isEqualTo(referenceLink);
        assertThat(deployment.getDeployedOn()).isNull();
        assertThat(deployment.isActive()).isEqualTo(false);
    }
    @Test
    void given_deployedOn_should_set_deployedOn_and_active(){
        final var deployedOn="https://deployed";
        final var deployment = new Deployment();

        deployment.deploy(deployedOn);

        assertThat(deployment.getDeployedOn()).isEqualTo(deployedOn);
        assertThat(deployment.isActive()).isEqualTo(true);
    }
    @Test
    void given_nothing_should_undeploy(){
        final var deployedOn="https://deployed";
        final var deployment = new Deployment();

        deployment.deploy(deployedOn);

        deployment.undeploy();
        assertThat(deployment.getDeployedOn()).isNull();
        assertThat(deployment.isActive()).isEqualTo(false);

    }
    @Test
    void given_already_deployed_should_throw_exception_when_deployed_again(){
        final var deployedOn1="https://deployed1";
        final var deployedOn2="https://deployed2";
        final var deployment = new Deployment();

        deployment.deploy(deployedOn1);

        assertThrows(IllegalStateException.class,()->deployment.deploy(deployedOn2));

    }
    @Test
    void given_already_deployed_should_redeploy_with_force_flag(){
        final var deployedOn1="https://deployed1";
        final var deployedOn2="https://deployed2";
        final var deployment = new Deployment();

        deployment.deploy(deployedOn1);
        deployment.deploy(deployedOn2,true);

        assertThat(deployment.getDeployedOn()).isEqualTo(deployedOn2);
        assertThat(deployment.isActive()).isEqualTo(true);

    }
}
