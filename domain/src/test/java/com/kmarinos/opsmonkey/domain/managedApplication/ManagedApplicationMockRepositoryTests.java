package com.kmarinos.opsmonkey.domain.managedApplication;

import com.kmarinos.opsmonkey.domain.model.ManagedApplication;
import com.kmarinos.opsmonkey.domain.repository.mock.ManagedApplicationMockRepositoryImpl;
import com.kmarinos.opsmonkey.domain.repository.mock.MockedData;
import io.github.artsok.RepeatedIfExceptionsTest;
import org.hibernate.cfg.NotYetImplementedException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class ManagedApplicationMockRepositoryTests {

    transient MockedData mockedData;
    transient Map<Long,ManagedApplication> repository;
    transient ManagedApplicationMockRepositoryImpl underTest;

    @BeforeEach
    void
     initTest(){
        mockedData=new MockedData();
        repository=mockedData.getMockApplicationData();
        underTest=new ManagedApplicationMockRepositoryImpl(repository);
    }
    @Test
    void
     given_remote_project_id_should_return_application(){
        final var app = repository.values().iterator().next();
        final var remoteProjectId= app.getRemoteProjectId();

        assertThat(underTest.findManagedApplicationByRemoteProjectIdEquals(remoteProjectId)).hasValue(app);
    }
    @Test
    void
     given_nothing_should_return_all_applications(){
        assertThat(underTest.findAll()).containsAll(repository.values());
    }
    @Test
    void
     given_sort_in_findAll_should_throw_nyi_exception(){
        assertThrows(NotYetImplementedException.class,()->underTest.findAll(Sort.unsorted()));
    }
    @Test
    void
     given_pageable_in_findAll_should_throw_nyi_exception(){
        assertThrows(NotYetImplementedException.class,()->underTest.findAll(Pageable.unpaged()));
    }
    @Test
    void
     given_ids_in_findAllById_should_throw_nyi_exception(){
        assertThrows(NotYetImplementedException.class,()->underTest.findAllById(new ArrayList<>()));
    }
    @Test
    void
     given_nothing_should_return_total_entries(){
        assertThat(underTest.count()).isEqualTo(repository.size());
    }
    @Test
    void
     given_id_should_delete_application(){
        final var id=repository.values().iterator().next().getId();

        underTest.deleteById(id);

        assertThat(repository).doesNotContainKey(id);
    }
    @Test
    void
     given_application_should_delete_application(){
        final var toDelete=repository.values().iterator().next();

        underTest.delete(toDelete);

        assertThat(repository).doesNotContainValue(toDelete);
    }
    @Test
    void
     given_nothing_in_deleteAllById_should_throw_nyi_exception(){
        assertThrows(NotYetImplementedException.class,()->underTest.deleteAllById(new ArrayList<>()));
    }
    @Test
    void
     given_applications_should_delete_applications(){
        final var iterator = repository.values().iterator();
        final var toDelete1= iterator.next();
        final var toDelete2= iterator.next();

        underTest.deleteAll(Arrays.asList(toDelete1,toDelete2));

        assertThat(repository).doesNotContainValue(toDelete1);
        assertThat(repository).doesNotContainValue(toDelete2);
    }
    @Test
    void
     given_nothing_in_deleteAll_should_throw_nyi_exception(){
        assertThrows(NotYetImplementedException.class,()->underTest.deleteAll());
    }
    @Test
    void
     given_application_should_save_application(){
        final var toSave = ManagedApplication.builder().build();
        int totalEntities = repository.size();

        underTest.save(toSave);

        assertThat(repository).containsValue(toSave);
        assertThat(repository.size()).isEqualTo(totalEntities+1);
    }
    @Test
    void
     given_applications_should_save_applications(){
        final var toSave1 = ManagedApplication.builder().build();
        final var toSave2 = ManagedApplication.builder().build();
        int totalEntities = repository.size();
        underTest.saveAll(Arrays.asList(toSave1,toSave2));

        assertThat(repository).containsValues(toSave1,toSave2);
        assertThat(repository.size()).isEqualTo(totalEntities+2);
    }
    @Test
    void
     given_id_should_return_application(){
        final var toFind = repository.values().iterator().next();
        final var id= toFind.getId();
        assertThat(underTest.findById(id)).hasValue(toFind);
    }
    @Test
    void
     given_missing_id_should_return_empty(){
        final var missingId=-1L;
        assertThat(underTest.findById(missingId)).isEmpty();
    }
    @Test
    void
     given_existing_id_should_return_true(){
        final var id = repository.keySet().iterator().next();
        assertThat(underTest.existsById(id)).isTrue();
    }
    @Test
    void
     given_nothing_in_flush_should_throw_nyi_exception(){
        assertThrows(NotYetImplementedException.class,()->underTest.flush());
    }
    @Test
    void
     given_application_should_save_application_in_saveAndFlush(){
        final var toSave = ManagedApplication.builder().build();
        int totalEntities = repository.size();

        underTest.saveAndFlush(toSave);

        assertThat(repository).containsValue(toSave);
        assertThat(repository.size()).isEqualTo(totalEntities+1);
    }
    @Test
    void
     given_applications_should_save_applications_in_saveAllAndFlush(){
        final var toSave1 = ManagedApplication.builder().build();
        final var toSave2 = ManagedApplication.builder().build();
        int totalEntities = repository.size();

        underTest.saveAllAndFlush(Arrays.asList(toSave1,toSave2));

        assertThat(repository).containsValues(toSave1,toSave2);
        assertThat(repository.size()).isEqualTo(totalEntities+2);
    }
    @Test
    void
     given_applications_should_delete_applications_in_deleteAllInBatch(){
        final var iterator = repository.values().iterator();
        final var toDelete1= iterator.next();
        final var toDelete2= iterator.next();

        underTest.deleteAllInBatch(Arrays.asList(toDelete1,toDelete2));

        assertThat(repository).doesNotContainValue(toDelete1);
        assertThat(repository).doesNotContainValue(toDelete2);
    }
    @Test
    void
     given_ids_in_deleteAllByIdInBatch_should_throw_nyi_exception(){
        assertThrows(NotYetImplementedException.class,()->underTest.deleteAllByIdInBatch(new ArrayList<>()));
    }
    @Test
    void
     given_nothing_in_deleteAllInBatch_should_throw_nyi_exception(){
        assertThrows(NotYetImplementedException.class,()->underTest.deleteAllInBatch());
    }
    @Test
    void
     given_id_in_getOne_should_throw_nyi_exception(){
        assertThrows(NotYetImplementedException.class,()->underTest.getOne(1L));
    }
    @Test
    void
     given_id_should_return_application_in_getById(){
        final var toFind = repository.values().iterator().next();
        final var existingId= toFind.getId();
        assertThat(underTest.getById(existingId)).isEqualTo(toFind);
    }
    @Test
    void
     given_example_application_in_findOne_should_throw_nyi_exception(){
        assertThrows(NotYetImplementedException.class,()->underTest.findOne(Example.of(ManagedApplication.builder().build())));
    }
    @Test
    void
     given_example_application_in_findAll_should_throw_nyi_exception(){
        assertThrows(NotYetImplementedException.class,()->underTest.findAll(Example.of(ManagedApplication.builder().build())));
    }
    @Test
    void
     given_example_application_and_sort_in_findAll_should_throw_nyi_exception(){
        assertThrows(NotYetImplementedException.class,
                ()->underTest.findAll(Example.of(ManagedApplication.builder().build()),Sort.unsorted()));
    }
    @Test
    void
     given_example_application_and_pageable_in_findAll_should_throw_nyi_exception(){
        assertThrows(NotYetImplementedException.class,
                ()->underTest.findAll(Example.of(ManagedApplication.builder().build()),Pageable.unpaged()));
    }
    @Test
    void
     given_example_application_in_count_should_throw_nyi_exception(){
        assertThrows(NotYetImplementedException.class,
                ()->underTest.count(Example.of(ManagedApplication.builder().build())));
    }
    @Test
    void
     given_example_application_in_exists_should_throw_nyi_exception(){
        assertThrows(NotYetImplementedException.class,
                ()->underTest.exists(Example.of(ManagedApplication.builder().build())));
    }
    @Test
    void
     given_example_application_and_query_in_findBy_should_throw_nyi_exception(){
        assertThrows(NotYetImplementedException.class,
                ()->underTest.findBy(Example.of(ManagedApplication.builder().build()),
                        Function.identity()));
    }

}
