package com.kmarinos.opsmonkey.environment.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.kmarinos.opsmonkey.application.api.ManagedApplicationGET;
import com.kmarinos.opsmonkey.deployment.api.DeploymentGET;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class EnvironmentGET {

    String id;
    String name;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    ManagedApplicationGET application;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    List<DeploymentGET> deployments;

}
