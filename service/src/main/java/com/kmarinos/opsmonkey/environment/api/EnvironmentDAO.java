package com.kmarinos.opsmonkey.environment.api;

import com.kmarinos.opsmonkey.application.api.ManagedApplicationDAO;
import com.kmarinos.opsmonkey.application.api.ManagedApplicationGET;
import com.kmarinos.opsmonkey.deployment.api.DeploymentDAO;
import com.kmarinos.opsmonkey.deployment.api.DeploymentGET;
import com.kmarinos.opsmonkey.domain.model.Deployment;
import com.kmarinos.opsmonkey.domain.model.Environment;
import com.kmarinos.opsmonkey.domain.model.ManagedApplication;
import lombok.Data;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Data
public class EnvironmentDAO {

    public static List<EnvironmentGET> GET(List<Environment> environments) {
        return environments.stream().map(environment -> GET(environment,
                        ManagedApplicationDAO::referenced,
                        DeploymentDAO::GET))
                .collect(Collectors.toList());
    }

    public static EnvironmentGET GET(Environment environment) {
        return GET(environment,
                ManagedApplicationDAO::referenced,
                DeploymentDAO::GET);
    }

    private static EnvironmentGET GET(Environment environment,
                                      Function<ManagedApplication, ManagedApplicationGET> handleApp,
                                      Function<Collection<Deployment>, List<DeploymentGET>> handleDeployments) {
        return buildSimple(environment)
                .application(handleApp.apply(environment.getManagedApplication()))
                .deployments(handleDeployments.apply(environment.getDeployments()))
                .build();
    }
    private static EnvironmentGET.EnvironmentGETBuilder buildSimple(Environment environment){
        return EnvironmentGET.builder()
                .id("" + environment.getId())
                .name(environment.getName());
    }

    public static EnvironmentGET referenced(Environment environment) {
        return GET(environment, application -> null, deployments -> null);
    }

}
