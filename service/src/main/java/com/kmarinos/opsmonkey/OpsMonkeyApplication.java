package com.kmarinos.opsmonkey;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class OpsMonkeyApplication {
	public static void main(String[] args) {
		SpringApplication.run(OpsMonkeyApplication.class, args);
	}
}
