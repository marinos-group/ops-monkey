package com.kmarinos.opsmonkey.application.api;

import com.kmarinos.opsmonkey.deployment.api.DeploymentDAO;
import com.kmarinos.opsmonkey.domain.model.ManagedApplication;

import java.util.List;
import java.util.stream.Collectors;

public class ManagedApplicationDAO {

    public static List<ManagedApplicationGET> GET(List<ManagedApplication> applications){
        return applications.stream()
                .map(application->ManagedApplicationDAO.buildSimple(application)
                        .activeDeployments(DeploymentDAO.GET(application.getActiveDeployments())))
                .map(ManagedApplicationGET.ManagedApplicationGETBuilder::build).collect(Collectors.toList());
    }
    public static ManagedApplicationGET GET(ManagedApplication application){
        return ManagedApplicationDAO.buildSimple(application)
                .activeDeployments(DeploymentDAO.GET(application.getActiveDeployments()))
                .build();
    }
    public static ManagedApplicationGET referenced(ManagedApplication application){
        return ManagedApplicationDAO.buildSimple(application).build();
    }
    private static ManagedApplicationGET.ManagedApplicationGETBuilder buildSimple(ManagedApplication application){
        return ManagedApplicationGET.builder()
                .id(""+application.getId())
                .remoteProjectId(application.getRemoteProjectId());
    }
}
