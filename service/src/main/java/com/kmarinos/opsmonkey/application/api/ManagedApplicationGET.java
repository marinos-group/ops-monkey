package com.kmarinos.opsmonkey.application.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.kmarinos.opsmonkey.deployment.api.DeploymentGET;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
public class ManagedApplicationGET {

    String id;
    String remoteProjectId;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    List<DeploymentGET> activeDeployments;
}
