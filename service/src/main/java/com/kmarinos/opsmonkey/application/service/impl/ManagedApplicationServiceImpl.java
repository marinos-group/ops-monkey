package com.kmarinos.opsmonkey.application.service.impl;

import com.kmarinos.opsmonkey.application.service.ManagedApplicationService;
import com.kmarinos.opsmonkey.domain.model.ManagedApplication;
import com.kmarinos.opsmonkey.domain.repository.ManagedApplicationRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
//@Profile({"!mock & !test"})
public class ManagedApplicationServiceImpl implements ManagedApplicationService {
    private final ManagedApplicationRepository managedApplicationRepository;

    public ManagedApplication registerNewApplication(String remoteProjectId) {
        return managedApplicationRepository.save(
                ManagedApplication.builder()
                        .remoteProjectId(remoteProjectId)
                        .build()
        );
    }

    public ManagedApplication getApplicationByRemoteId(String remoteProjectId) {
        return managedApplicationRepository
                .findManagedApplicationByRemoteProjectIdEquals(remoteProjectId).orElseThrow(()->new RuntimeException("Project not found."));
    }

    @Override
    public List<ManagedApplication> getAllApplications() {
        return this.managedApplicationRepository.findAll();
    }

    @Override
    public ManagedApplication getApplicationById(Long id) {
        return this.managedApplicationRepository.findById(id).orElseThrow(()->new RuntimeException("Application not found."));
    }


}
