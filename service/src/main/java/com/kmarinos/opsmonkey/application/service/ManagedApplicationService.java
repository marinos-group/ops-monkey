package com.kmarinos.opsmonkey.application.service;


import com.kmarinos.opsmonkey.domain.model.ManagedApplication;

import java.util.List;

public interface ManagedApplicationService {
    public ManagedApplication registerNewApplication(String remoteProjectId);
    public ManagedApplication getApplicationByRemoteId(String remoteProjectId);
    public List<ManagedApplication> getAllApplications();
    public ManagedApplication getApplicationById(Long id);
}
