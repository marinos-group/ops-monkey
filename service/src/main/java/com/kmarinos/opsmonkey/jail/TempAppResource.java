package com.kmarinos.opsmonkey.jail;

import com.kmarinos.opsmonkey.application.api.ManagedApplicationDAO;
import com.kmarinos.opsmonkey.application.api.ManagedApplicationGET;
import com.kmarinos.opsmonkey.application.service.ManagedApplicationService;
import com.kmarinos.opsmonkey.deployment.service.DeploymentService;
import com.kmarinos.opsmonkey.domain.model.Deployment;
import com.kmarinos.opsmonkey.domain.model.ManagedApplication;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/ops-monkey")
public class TempAppResource {

    private final DeploymentService deploymentService;
    private final ManagedApplicationService managedApplicationService;

    @GetMapping("api/apps")
    public ResponseEntity<List<ManagedApplicationGET>> getAllApps(){
        return ResponseEntity.ok(ManagedApplicationDAO.GET(managedApplicationService.getAllApplications()));
    }

    @GetMapping("apps/{appId}/{deploymentId}")
    public void redirectForDeployment(@PathVariable("appId")Long appId, @PathVariable("deploymentId")Long deploymentId, HttpServletResponse response) throws IOException {
//        Deployment deployment=deploymentService.getById(deploymentId);
        log.info("Received request. Redirecting...");
        response.sendRedirect("https://google.com");
    }
    @PostMapping("{projectId}")
    public ResponseEntity<String> registerApplication(@RequestParam("env") String environment, @PathVariable("projectId") String projectId, HttpServletRequest request, HttpServletResponse response) {

        ManagedApplication app = managedApplicationService.getApplicationByRemoteId(projectId);
        Deployment deployment = deploymentService.registerNewDeployment(app, environment);


        log.info("Received deploy signal for project with id \"{}\" and environment {}", projectId, environment);
        log.info("Context path: {}", request.getContextPath());
        logRequestHeaders(request);
        log.info("Path info: {}", request.getPathInfo());
        this.logCookies(request);
//        processParts(request);
        log.info("{}", "");
        return ResponseEntity.ok(deployment.getReferenceLink());
    }
    private void logCookies(HttpServletRequest request) {
        if (request.getCookies() == null) {
            log.info("Cookies: ");
        } else {
            log.info("Cookies: {}", Arrays.stream(request.getCookies()).map(c -> c.getName() + ":" + c.getValue()).collect(Collectors.joining(", ")));
        }
    }
    private void logRequestHeaders(HttpServletRequest request) {
        log.info("Headers in request: {}", StreamSupport.stream(
                Spliterators.spliteratorUnknownSize(
                        request.getHeaderNames().asIterator(),
                        Spliterator.ORDERED)
                , false).map(h -> h + ":" + request.getHeader((String) h)).collect(Collectors.joining(", ")));
    }
}
