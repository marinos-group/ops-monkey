package com.kmarinos.opsmonkey.jail;

import com.kmarinos.opsmonkey.jail.commandexecution.CommandExecutor;
import com.kmarinos.opsmonkey.jail.commandexecution.LoggingCommandExecutor;
import com.kmarinos.opsmonkey.jail.commandexecution.PowershellCommandExecutor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.SystemUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.lang.reflect.InvocationTargetException;

@Configuration
@Slf4j
public class AppConfig {

    @Bean
    public CommandExecutor getCommandExecutorForOS() {
        try {
            if (SystemUtils.IS_OS_WINDOWS) {
                return this.logAndCreate(PowershellCommandExecutor.class);
            }else if(SystemUtils.IS_OS_LINUX){
                this.logAndCreate(LoggingCommandExecutor.class);
            }
        } catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
            log.error("Cannot create CommandExecutor...");
        }
        return new LoggingCommandExecutor();
    }

    private CommandExecutor logAndCreate(Class<? extends CommandExecutor> exec) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        log.info("Found operating system: {}. Returning {}", SystemUtils.OS_NAME, exec.getSimpleName());
        return exec.getConstructor().newInstance();
    }
}
