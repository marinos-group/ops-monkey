package com.kmarinos.opsmonkey.jail.commandexecution;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;


public class PowershellCommandExecutor implements CommandExecutor{
    @Override
    public String executeLine(String command) {
        command = String.format("powershell.exe %s",command);
        return new ExecutionContext().execute(command).getAllStdLines();

    }
    @Slf4j
    private static class ExecutionContext{
        Map<String, List<String>> lines;
        public String getAllStdLines(){
            return lines.values().stream().flatMap(List::stream).collect(Collectors.joining(System.lineSeparator()));
        }
        public ExecutionContext(){
            lines = new HashMap<>();
            lines.put("out",new ArrayList<>());
            lines.put("err",new ArrayList<>());
        }
        public  ExecutionContext execute(String command) {
            // Executing the command
            try {
                Process powerShellProcess = null;
                powerShellProcess = Runtime.getRuntime().exec(command);
                // Getting the results
                powerShellProcess.getOutputStream().close();
                String line;
                BufferedReader stdout = new BufferedReader(new InputStreamReader(
                        powerShellProcess.getInputStream()));
                while ((line = stdout.readLine()) != null) {
                    lines.get("out").add(line);
                }
                stdout.close();
                log.debug("Standard Error:");
                BufferedReader stderr = new BufferedReader(new InputStreamReader(
                        powerShellProcess.getErrorStream()));
                while ((line = stderr.readLine()) != null) {
                    lines.get("err").add(line);
                }
                stderr.close();
                powerShellProcess.destroy();
            } catch (IOException e) {
                lines.get("err").add(e.getMessage());
                Arrays.stream(e.getStackTrace()).forEach(s->lines.get("err").add(s.toString()));
            }
            lines.get("out").forEach(log::info);
            lines.get("err").forEach(log::error);
            return this;
        }
    }


}
