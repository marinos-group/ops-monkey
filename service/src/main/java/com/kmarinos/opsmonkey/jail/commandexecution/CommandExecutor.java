package com.kmarinos.opsmonkey.jail.commandexecution;

public interface CommandExecutor {
    public String executeLine(String command);
}
