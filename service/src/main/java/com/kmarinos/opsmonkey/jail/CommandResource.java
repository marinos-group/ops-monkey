package com.kmarinos.opsmonkey.jail;

import com.kmarinos.opsmonkey.jail.commandexecution.CommandExecutor;
import com.kmarinos.opsmonkey.domain.model.CommandExecution;
import com.kmarinos.opsmonkey.domain.model.CommandResult;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/execute")
@RequiredArgsConstructor
public class CommandResource {
    private final CommandExecutor commandExecutor;

    @PostMapping
    public ResponseEntity<CommandResult> executeCommand(@RequestBody CommandExecution command){
        return ResponseEntity.ok(CommandResult.builder().stdout(commandExecutor.executeLine(command.getCommand())).build());
    }
}
