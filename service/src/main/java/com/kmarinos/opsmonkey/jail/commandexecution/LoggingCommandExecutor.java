package com.kmarinos.opsmonkey.jail.commandexecution;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LoggingCommandExecutor implements CommandExecutor{
    @Override
    public String executeLine(String command) {
        log.info("Executing command...\"{}\"",command);
        return "Command logged from dummy executor.";
    }
}
