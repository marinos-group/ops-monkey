package com.kmarinos.opsmonkey;

import com.kmarinos.opsmonkey.domain.model.ManagedApplication;
import com.kmarinos.opsmonkey.domain.repository.ManagedApplicationRepository;
import com.xebialabs.overthere.CmdLine;
import com.xebialabs.overthere.OverthereConnection;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
@Profile("dev")
public class InitDev {

    private final ManagedApplicationRepository managedApplicationRepository;

    @EventListener(ApplicationReadyEvent.class)
    public void init(){
        ManagedApplication app1=ManagedApplication.builder()
                .remoteProjectId("12")
                .build();
        ManagedApplication app2=ManagedApplication.builder()
                .remoteProjectId("13")
//                .deployments(new HashSet<>())
                .build();
        managedApplicationRepository.save(app1);
//        managedApplicationRepository.save(app2).block();
        log.error("Init..."+managedApplicationRepository.count());
    }
    /*@EventListener(ApplicationReadyEvent.class)
    public void testOverThere(){
        ConnectionOptions options = new ConnectionOptions();
        options.set(ConnectionOptions.ADDRESS, "pve-mgmt.marinos.com");
        options.set("sudoUsername", "criux");
        options.set("username", "criux");
        options.set(ConnectionOptions.PASSWORD, "Criux#1789kdm");
//        options.set(SshConnectionBuilder.PRIVATE_KEY_FILE,"C:\\Users\\KMarinos\\.ssh\\id_rsa");
        options.set(ConnectionOptions.OPERATING_SYSTEM, OperatingSystemFamily.UNIX);
        options.set(SshConnectionBuilder.CONNECTION_TYPE, SshConnectionType.SFTP);
        try (OverthereConnection connection = Overthere.getConnection("ssh", options)) {
            this.connection=connection;
//            connection.execute(CmdLine.build("ping","-c 13", "google.com"));
//            connection.execute(CmdLine.build("ping","-c 13", "twitch.tv"));
            execute("(./script.sh & )> /dev/pts/2");
            System.out.println("done...");

        }
    }*/
    OverthereConnection connection;
    private void execute(String command){
        connection.execute(new CmdLine().addRaw(command));
    }
    /*@EventListener(ApplicationReadyEvent.class)
    public void testSshj() throws IOException {
        final SSHClient ssh = new SSHClient();
        ssh.loadKnownHosts();
        ssh.connect("pve-mgmt.marinos.com");
        Session session = null;
        try {
            ssh.authPublickey("criux");
            session = ssh.startSession();
//            final Session.Command cmd = session.exec("ping -c 3 google.com");
//
//            cmd.getInputStream().transferTo(System.out);
//            cmd.join(5, TimeUnit.SECONDS);
//            System.out.print("\n** exit status: " + cmd.getExitStatus());
//            Session.Command cmd2 = session.exec("ls");
//            cmd2.getInputStream().transferTo(System.out);
//            cmd2.join();
//            System.out.print("\n** exit status: " + cmd2.getExitStatus());
            Session.Shell shell = session.startShell();
            shell.getInputStream().transferTo(System.out);
            shell.getOutputStream().write("ping -c 3 google.com".getBytes(StandardCharsets.UTF_8));
            shell.getOutputStream().close();
            System.out.println(new String(shell.getInputStream().readAllBytes()));
            shell.getOutputStream().flush();

            shell.join();
        } finally {
            try {
                if (session != null) {
                    session.close();
                }
            } catch (IOException e) {
                // Do Nothing
            }

            ssh.disconnect();
        }
    }*/
}
