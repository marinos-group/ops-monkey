package com.kmarinos.opsmonkey.deployment.api;

import com.kmarinos.opsmonkey.application.api.ManagedApplicationDAO;
import com.kmarinos.opsmonkey.application.api.ManagedApplicationGET;
import com.kmarinos.opsmonkey.domain.model.Deployment;
import com.kmarinos.opsmonkey.domain.model.Environment;
import com.kmarinos.opsmonkey.domain.model.ManagedApplication;
import com.kmarinos.opsmonkey.environment.api.EnvironmentDAO;
import com.kmarinos.opsmonkey.environment.api.EnvironmentGET;
import lombok.Data;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Data
public class DeploymentDAO {
    public static List<DeploymentGET> GET(Collection<Deployment> deployments) {
        return deployments.stream().map(deployment -> DeploymentDAO.GET(deployment,
                        ManagedApplicationDAO::referenced,
                        EnvironmentDAO::referenced))
                .collect(Collectors.toList());
    }

    public static DeploymentGET GET(Deployment deployment) {
        return DeploymentDAO.GET(deployment,
                ManagedApplicationDAO::GET,
                EnvironmentDAO::referenced);
    }

    private static DeploymentGET GET(Deployment deployment,
                                     Function<ManagedApplication, ManagedApplicationGET> handleApp,
                                     Function<Environment, EnvironmentGET> handleEnv) {
        return DeploymentDAO.buildSimple(deployment)
                .managedApplication(handleApp.apply(deployment.getManagedApplication()))
                .environment(handleEnv.apply(deployment.getEnvironment()))
                .build();
    }

    private static DeploymentGET.DeploymentGETBuilder buildSimple(Deployment deployment) {
        return DeploymentGET.builder()
                .id("" + deployment.getId())
                .referenceLink(deployment.getReferenceLink())
                .deployedOn(deployment.getDeployedOn())
                .active(deployment.isActive());
    }
}
