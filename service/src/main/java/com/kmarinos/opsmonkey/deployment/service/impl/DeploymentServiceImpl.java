package com.kmarinos.opsmonkey.deployment.service.impl;

import com.kmarinos.opsmonkey.domain.model.Deployment;
import com.kmarinos.opsmonkey.domain.model.ManagedApplication;
import com.kmarinos.opsmonkey.domain.repository.DeploymentRepository;
import com.kmarinos.opsmonkey.deployment.service.DeploymentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Service
@Slf4j
@RequiredArgsConstructor
public class DeploymentServiceImpl implements DeploymentService {

//    private final DeploymentRepository deploymentRepository;
    @Value("${server.address:}")
    String serverAddress;
    @Value("${server.port}")
    int serverPort;
    String serverProtocol="http";
    @Value("${server.servlet.context-path:}")
    String rootPath;

    public Deployment getById(Long deploymentId){
        return Deployment.builder().build();
       /* return deploymentRepository
                .findById(deploymentId).blockOptional()
                .orElseThrow(()-> new RuntimeException("Deployment not found"));*/
    }
    public String getRealDeploymentLink(Deployment deployment){
        deployment=this.getById(deployment.getId());
        if(deployment.getDeployedOn()==null||deployment.getDeployedOn().isEmpty()){
            deployment.deploy(this.generateDeploymentLink(deployment));
        }
        return deployment.getDeployedOn();
    }
    public Deployment registerNewDeployment(ManagedApplication application, String environment){
        Deployment toDeploy=Deployment.builder()
//                .application(application)
//                .environment(environment)
                .build();
        return toDeploy;
//        deploymentRepository.save(toDeploy);
//        toDeploy.setReferenceLink(String.format("%s/apps/%s/%s",this.getServerBaseUrl(),application.getId(),toDeploy.getId()));
//
//        return deploymentRepository.save(toDeploy).block();

    }
    private String generateDeploymentLink(Deployment deployment){
        return "https://google.com";
    }
    private String getServerBaseUrl(){
        if(serverAddress.isEmpty()){
            // Local address
            try {
                serverAddress=InetAddress.getLocalHost().getHostAddress();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }
        return String.format("%s://%s:%s%s",serverProtocol,serverAddress,serverPort,rootPath);
    }
}
