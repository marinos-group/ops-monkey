package com.kmarinos.opsmonkey.deployment.service;


import com.kmarinos.opsmonkey.domain.model.Deployment;
import com.kmarinos.opsmonkey.domain.model.ManagedApplication;

public interface DeploymentService {
    public Deployment getById(Long deploymentId);
    public String getRealDeploymentLink(Deployment deployment);
    public Deployment registerNewDeployment(ManagedApplication application, String environment);
}
