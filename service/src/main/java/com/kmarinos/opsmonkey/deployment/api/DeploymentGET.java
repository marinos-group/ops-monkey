package com.kmarinos.opsmonkey.deployment.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.kmarinos.opsmonkey.application.api.ManagedApplicationGET;
import com.kmarinos.opsmonkey.environment.api.EnvironmentGET;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DeploymentGET {

    String id;
    String referenceLink;
    String deployedOn;
    boolean active;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    ManagedApplicationGET managedApplication;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    EnvironmentGET environment;

}
