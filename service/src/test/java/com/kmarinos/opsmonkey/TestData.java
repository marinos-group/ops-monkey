package com.kmarinos.opsmonkey;

import com.kmarinos.opsmonkey.domain.model.Deployment;
import com.kmarinos.opsmonkey.domain.model.Environment;
import com.kmarinos.opsmonkey.domain.model.ManagedApplication;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TestData {

    public static List<ManagedApplication> getSomeApplications() {
        return TestData.getApplications(5);
    }

    public static ManagedApplication getSampleApplication() {
        return getApplicationWithId(1);
    }

    public static List<Environment> getSomeEnvironments() {
        return TestData.getSomeEnvironments(null);
    }

    public static List<Environment> getSomeEnvironments(ManagedApplication app) {
        return TestData.getEnvironments(app, 5);
    }

    public static Environment getSampleEnvironment() {
        return getSampleEnvironment(null);
    }

    public static Environment getSampleEnvironment(ManagedApplication app) {
        return getEnvironmentWithId(app, 1);
    }

    public static List<Deployment> getSomeDeployments(ManagedApplication app, Environment env){
        return new ArrayList<>(TestData.generateDeployments(app,env,5));
    }
    public static List<Deployment> getSomeDeployments(ManagedApplication app){
        return getSomeDeployments(app,null);
    }
    public static List<Deployment> getSomeDeployments(Environment env){
        return getSomeDeployments(null,env);
    }
    public static List<Deployment> getSomeDeployments(){
        return getSomeDeployments(null,null);
    }

    public static Deployment getSampleDeployment(ManagedApplication app,Environment env){
        return TestData.getDeploymentWithId(app,env,1);
    }
    public static Deployment getSampleDeployment(ManagedApplication app){
        return TestData.getSampleDeployment(app,null);
    }
    public static Deployment getSampleDeployment(Environment env){
        return TestData.getSampleDeployment(null,env);
    }
    public static Deployment getSampleDeployment(){
        return TestData.getSampleDeployment(null,null);
    }

    private static ManagedApplication getApplicationWithId(int id) {
        final var app = ManagedApplication.builder()
                .id((long) id)
                .remoteProjectId("Project-" + id)
                .build();
        app.setActiveDeployments(generateRandomDeployments(app));
        return app;
    }

    private static List<ManagedApplication> getApplications(int total) {
        return IntStream.range(1, total + 1).mapToObj(TestData::getApplicationWithId).collect(Collectors.toList());
    }

    private static Environment getEnvironmentWithId(final ManagedApplication app, int id) {
        final var env = Environment.builder()
                .name("env-" + id)
                .managedApplication(app)
                .build();
        env.setDeployments(generateRandomDeployments(app, env));
        return null;
    }

    private static List<Environment> getEnvironments(final ManagedApplication app, int total) {
        return IntStream.range(1, total + 1).mapToObj(idx -> TestData.getEnvironmentWithId(app, idx)).collect(Collectors.toList());
    }

    private static Set<Deployment> generateRandomDeployments(final ManagedApplication app, final Environment env) {
        return TestData.generateDeployments(app, env, ThreadLocalRandom.current().nextInt(0, 3));
    }

    private static Set<Deployment> generateRandomDeployments(final ManagedApplication app) {
        return TestData.generateDeployments(app, ThreadLocalRandom.current().nextInt(0, 3));
    }

    private static Set<Deployment> generateDeployments(final ManagedApplication app, final Environment env, int total) {
        return IntStream.range(1, total + 1).mapToObj(idx -> TestData.getDeploymentWithId(app, env, idx)).collect(Collectors.toSet());
    }

    private static Set<Deployment> generateDeployments(final ManagedApplication app, int total) {
        return IntStream.range(1, total + 1).mapToObj(idx -> TestData.getDeploymentWithId(app, idx)).collect(Collectors.toSet());
    }

    private static Deployment.DeploymentBuilder getDeploymentBuilderWithId(int id) {
        return Deployment.builder()
                .id((long) id)
                .deployedOn("https://deployed/" + id)
                .referenceLink("https://reference/" + 1);
    }

    private static Deployment getDeploymentWithId(final ManagedApplication app, int id) {
        return getDeploymentWithId(app, null, id);
    }

    private static Deployment getDeploymentWithId(final ManagedApplication app, final Environment env, int id) {
        return getDeploymentBuilderWithId(id)
                .managedApplication(app)
                .environment(env)
                .build();
    }
}
