package com.kmarinos.opsmonkey.application;

import com.kmarinos.opsmonkey.TestData;
import com.kmarinos.opsmonkey.application.service.impl.ManagedApplicationServiceImpl;
import com.kmarinos.opsmonkey.domain.model.ManagedApplication;
import com.kmarinos.opsmonkey.domain.repository.ManagedApplicationRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ManagedApplicationServiceTests {

    @InjectMocks
    ManagedApplicationServiceImpl managedApplicationService;

    @Mock
    private ManagedApplicationRepository managedApplicationRepository;

    @Test
    public void given_nothing_should_return_all_managedApplications() {
        List<ManagedApplication> apps= TestData.getSomeApplications();
        when(managedApplicationRepository.findAll()).thenReturn(apps);
        assertThat(managedApplicationService.getAllApplications()).containsAll(apps);
    }
    @Test
    public void given_existing_id_should_return_application(){
        final var sample = TestData.getSampleApplication();
        long id= sample.getId();
        when(managedApplicationRepository.findById(id)).thenReturn(Optional.of(sample));
        assertThat(managedApplicationService.getApplicationById(id)).isSameAs(sample);
    }
    @Test
    public void given_missing_id_should_return_runtime_exception(){
        long missingId=1L;
        when(managedApplicationRepository.findById(missingId)).thenReturn(Optional.empty());
        assertThrows(RuntimeException.class,()->managedApplicationService.getApplicationById(missingId));
    }
    @Test
    public void given_existing_remote_project_id_should_return_application(){
        final var sample = TestData.getSampleApplication();
        String projectId= sample.getRemoteProjectId();
        when(managedApplicationRepository.findManagedApplicationByRemoteProjectIdEquals(projectId)).thenReturn(Optional.of(sample));
        assertThat(managedApplicationService.getApplicationByRemoteId(projectId)).isSameAs(sample);
    }
    @Test
    public void given_missing_remote_project_id_should_return_runtime_exception(){
        String missingProjectId="Project-1";
        when(managedApplicationRepository.findManagedApplicationByRemoteProjectIdEquals(missingProjectId)).thenReturn(Optional.empty());
        assertThrows(RuntimeException.class,()->managedApplicationService.getApplicationByRemoteId(missingProjectId));
    }
    @Test
    public void given_new_remote_project_id_should_create_application(){
        final var sample = TestData.getSampleApplication();
        long id=sample.getId();
        String projectId= sample.getRemoteProjectId();
        when(managedApplicationRepository.save(Mockito.any(ManagedApplication.class))).thenAnswer(i->{
            final var app = (ManagedApplication)i.getArguments()[0];
            app.setId(id);
            return app;
        });
        assertThat(managedApplicationService.registerNewApplication(projectId)).isEqualTo(sample);
    }
}
